#pragma once
#include <string>
#include <iostream>
using namespace std;
class Morse_Data
{
public:
	char letter; // alphabet letter
	string code; // morse code for that letter
	Morse_Data(string c = "", char l = '/0') : code(c), letter(l) {}

	friend ostream& operator<<(ostream& out, Morse_Data& data);
	// outputs the letter, used for tree verification

	friend bool operator<(const Morse_Data& lhs, const Morse_Data& rhs);
	// compares Morse_Data codes, . < _

};

const ostream& operator<<(ostream& out, const Morse_Data& data)
{
	out << data.letter;
	return out;
}

bool operator<(const Morse_Data& lhs, const Morse_Data& rhs)
{
	string left_code = lhs.code;
	string right_code = rhs.code;
	if (right_code == "") // comparing with root
	{
		if (left_code[0] == '.')
		{
			return true; // go left from root
		}
		return false;
	}
	if (left_code == "") // comparing with root
	{
		if (right_code[0] == '_')
		{
			return true; // go right from root
		}
		return false;
	}
	if (left_code.length() > right_code.length())
	{
		for (int i = 0; i < right_code.length(); i++)
		{
			if (left_code[i] == right_code[i])
			{
				continue;
			}
			else if (left_code[i] == '.' && right_code[i] == '_')
			{
				return true; // go left
			}
			else
			{
				return false; // go right
			}
		}
		// if last letter is . go left
		if (left_code[right_code.length()] == '.')
		{
			return true;
		}
		return false;
	}
	else if (right_code.length() > left_code.length())
	{
		for (int i = 0; i < left_code.length(); i++)
		{
			if (left_code[i] == right_code[i])
			{
				continue;
			}
			else if (left_code[i] == '.' && right_code[i] == '_')
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		if (right_code[left_code.length()] == '.')
		{
			return false;
		}
		return true;
	}
	return false;
}