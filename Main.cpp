// Jackie Batson
// CS 303
// Project 3b
// December 4, 2015


#include "Morse_Code.h"

void test_encode(string word, string expected);
void test_decode(string word, string expected);

int main()
{
	// program will exit if morse.txt file not found or readable
	try
	{
		Morse_Code morse_code;
		string encode_result = morse_code.encode("Jackie");
		string decode_result = morse_code.decode(".___ ._ _._. _._ .. .");


		//// Testing entire alphabet, encode and decode
		//test_encode("The", "_ .... .");
		//test_encode("quick", "__._ .._ .. _._. _._");
		//test_encode("brown", "_... ._. ___ .__ _.");
		//test_encode("fox", ".._. ___ _.._");
		//test_encode("jumps", ".___ .._ __ .__. ...");
		//test_encode("over", "___ ..._ . ._.");
		//test_encode("the", "_ .... .");
		//test_encode("lazy", "._.. ._ __.. _.__");
		//test_encode("dog", "_.. ___ __.");
		//cout << endl;
		//test_decode("_ .... .", "the");
		//test_decode("__._ .._ .. _._. _._", "quick");
		//test_decode("_... ._. ___ .__ _.", "brown");
		//test_decode(".._. ___ _.._", "fox");
		//test_decode(".___ .._ __ .__. ...", "jumps");
		//test_decode("___ ..._ . ._.", "over");
		//test_decode("._.. ._ __.. _.__", "lazy");
		//test_decode("_.. ___ __.", "dog");

		//// Testing more capitals, lowercase returned
		//test_encode("QuIcK", "__._ .._ .. _._. _._");
		//cout << endl;

		//// Testing bad input for encoding, bad characters ignored
		//test_encode("the?", "_ .... .");
		//test_encode("l@zy", "._.. __.. _.__");
		//test_encode("!jumps", ".___ .._ __ .__. ...");
		//test_encode("d*o&g]", "_.. ___ __.");
		//cout << endl;

		//// Testing bad input for decoding, bad code sections ignored
		//test_decode("_.. ___ __. _____", "dog");
		//test_decode("___ !- ..._ . ._.", "over");
		//test_decode("_... ._. ___? .__ _.", "brwn");
		//test_decode("..?. ___ _.._", "ox");
	}
	// if morse.txt not found or opened
	catch (exception e)
	{
		cout << e.what() << endl;
		system("pause");
		return 1;
	}
	system("pause");
	return 0;	
}

// compares the returned value from a given input to the expected value
void test_encode(string word, string expected)
{
	Morse_Code morse_code;
	string result = morse_code.encode(word);
	if (result == expected)
	{
		cout << word << ": " << result << " : Pass" << endl;
	}
	else
	{
		cout << word << ": Fail. Expected " << expected << " got " << result << endl;
	}
}

// compares the returned value from a given input to the expected value
void test_decode(string code, string expected)
{
	Morse_Code morse_code;
	string result = morse_code.decode(code);
	if (result.compare(expected) == 0)
	{
		cout << code << ": " << result << " : Pass" << endl;
	}
	else
	{
		cout << code << ": Fail. Expected " << expected << " got " << result << endl;
	}
}



