#pragma once
#include <string>
#include <unordered_map>
#include <queue>
#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

#include "Morse_Data.h"
#include "Binary_Search_Tree.h"




class Morse_Code
{
public:
	Morse_Code() {}
	struct Position_Compare {
		// sorts Morse_Data according to the length of the code.  Results will allow for a min heap priority queue.
		bool operator()(Morse_Data lhs, Morse_Data rhs) const {
			string left_code = lhs.code;
			string right_code = rhs.code;
			return left_code.length() > right_code.length();
		}
	};

	void parse_file();
	// opens file morse.txt.  Builds a Morse_Data instance for each line in the file, with the first character as the
	// letter and the rest of the line as the code.  Only . and _ are allowable characters for the code, otherwise
	// the line is ignored. Morse_Data is pushed onto a priority queue and an unordered map, so morse_q and morse_map
	// are built in this method.

	Binary_Search_Tree<Morse_Data> build_tree();
	// Builds a binary search tree from morse_q.  Dashes branch left and dots branch right.  Will parse the file
	// if it hasn't been done already.  Returns the binary search tree containing Morse_Data.

	string encode(const string& word);
	// Input: string containing a word of English letters
	// Output: the Morse code for the word, with letters separated by spaces.
	// Uses morse_map to lookup letters, so will parse file if map has not been built.
	// If a given letter is not found in the map, that letter is ignored and the result is returned without it.

	string decode(const string& code);
	// Input: string containing morse code, letters separated by spaces
	// Output: a word containing English letters that is the code decoded.
	// Calls build_tree() to build the binary search tree for decoding.
	// If a given code is not valid (constructed of dots and underscores), then that letter is ignored and result
	// is returned without it.

	bool is_valid_code(const string& code);
	// returns true if code contains only . or _, false otherwise.

	
private:
	// queue sorted by the length of the Morse_Data code, needed to construct the appropriate binary tree.
	priority_queue<Morse_Data, vector<Morse_Data>, Position_Compare> morse_q;
	// unordered map for lookup by letter in the encode method.
	unordered_map<char, string> morse_map;

};


void Morse_Code::parse_file()
{
	stringstream ss;
	string line;
	ifstream fin("morse.txt");
	if (fin)
	{
		char letter;
		string code;
		int count = 1; // to keep track of line number
		while (getline(fin, line))
		{
			ss.clear();
			ss << line;
			ss >> letter >> code;
			// check to make sure the code has only . and _, otherwise the < comparator will not work properly.
			if (is_valid_code(code))
			{
				// build morse_map and morse_q
				Morse_Data data(code, letter);
				morse_q.push(data);
				pair<const char, string> morse_pair(data.letter, data.code);
				morse_map.insert(morse_pair);
			}
			else // ignore invalid codes in the file
			{
				cout << "Did not recognize " << code << ". Allowed characters are '_' and '.'. Line " << count << " ignored." << endl;
			}
			count++;
		}
		// verify morse_q
		/*while (!morse_q.empty()) {
			Morse_Data data = morse_q.top();
			cout << data.code << " " << data.letter << " " << data.code.size() << endl;
			morse_q.pop();
		}*/
	}
	else // morse.txt unable to be open
	{
		throw std::exception("Could not open file \"morse.txt\".");
	}
	fin.close();
}

Binary_Search_Tree<Morse_Data> Morse_Code::build_tree()
{
	// if encode() is called first, we do not need to parse again.
	if (morse_q.empty())
	{
		parse_file();
	}	
	Morse_Data blank("", '\0');
	Binary_Search_Tree<Morse_Data> morse_tree;
	morse_tree.insert(blank); // insert dummy node as root of morse_tree
	
	while (!morse_q.empty())
	{
		Morse_Data inserted = morse_q.top();
		morse_q.pop();
		// only valid codes will be inserted, since morse_q should not contain invalid characters
		morse_tree.insert(inserted); 
	}

	// verify tree
	/*string tree = morse_tree.pre_order();
	cout << tree;*/

	return morse_tree;	
}

string Morse_Code::encode(const string& word)
{
	string result = "";
	// if decode( ) is called first, should not need to parse file again.
	if (morse_map.empty())
	{
		parse_file();
	}
	for (int i = 0; i < word.length(); i++)
	{
		// Will encode capital letters to their lowercase equivalents
		char lower = tolower(word[i]);
		unordered_map<char, string>::iterator iter = morse_map.find(lower);
		if (iter != morse_map.end()) // found
		{
			if (i == word.length() - 1) // to avoid trailing space
			{
				result = result + iter->second;
			}
			else
			{
				result = result + iter->second + ' ';
			}
		}
		else // not found
		{
			cout << "Could not find char " << lower << " at index " << i << ", ignored." << endl;
			// eliminate null space that occurs when not found
			if (i == word.length() - 1)
			{
				result = result.substr(0, result.length() - 1);
			}			
		}
	}
	return result;
}

string Morse_Code::decode(const string& code)
{
	Binary_Search_Tree<Morse_Data> morse_tree = build_tree();
	string result = "";
	stringstream ss;
	ss << code;
	while (!ss.eof())
	{
		string letter_code; // string of dots and dashes for each letter
		ss >> letter_code;  // separated by space
		
		// must check if the code contains only . or _, since the existing find method will return the
		// local root if it can't branch left or right. Here, invalid codes are ignored. 
		if (is_valid_code(letter_code))
		{
			Morse_Data l_code(letter_code);
			const Morse_Data* found = morse_tree.find(l_code);
			if (found == nullptr || found->letter == 0)
			{
				cout << "Could not find " << letter_code << ", ignoring." << endl;
			}
			else
			{
				result = result + found->letter;
			}
		}
		else
		{
			cout << "Invalid code " << letter_code << ", ignoring." << endl;
			continue;
		}	
	}
	return result;
}

bool Morse_Code::is_valid_code(const string& code)
{
	for (int i = 0; i < code.length(); i++)
	{
		if (code[i] != '.' && code[i] != '_')
		{
			return false;
		}
	}
	return true;
}





