#ifndef MORSENODE_H_
#define MORSENODE_H_
#include <sstream>
#include <string>
using namespace std;

/** A node for a Binary Tree. */
struct Morse_Node
{
	// Data Fields
	char letter;
	string code;
	Morse_Node* left;
	Morse_Node* right;

	// Constructor
	Morse_Node(const char& l, const string& c,
		Morse_Node* left_val = NULL,
		Morse_Node* right_val = NULL) :
		letter(l), code (c), left(left_val), right(right_val) {}

	// Destructor (to avoid warning message)
	virtual ~Morse_Node() {}

	// to_string
	virtual string to_string() const {
		ostringstream os;
		os << code;
		return os.str();
	}
}; // End MorseNode

   // Overloading the ostream insertion operator
std::ostream& operator<<(std::ostream& out,
	const Morse_Node& node) {
	return out << node.to_string();
}

#endif